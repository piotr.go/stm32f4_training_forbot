/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <limits.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define LSM303_ACC_ADDRESS (0x19 << 1)  // Accelerometer address

#define LSM303_ACC_CTRL_REG1_A 0x20  // settings register

// CTRL_REG1_A = [ODR3][ODR2][ODR1][ODR0][LPEN][ZEN][YEN][XEN]
#define LSM303_ACC_Z_ENABLE 0x04  // 0000 0100
#define LSM303_ACC_XYZ_ENABLE 0x07  // 0000 0100
#define LSM303_ACC_100HZ 0x50  // 0101 0000

#define LSM303_ACC_REG_OUT_X_L_A 0x28  // low byte x axis register address
#define LSM303_ACC_REG_OUT_X_H_A 0x29  // high byte x axis register address
#define LSM303_ACC_REG_OUT_Y_L_A 0x2A  // low byte y axis register address
#define LSM303_ACC_REG_OUT_Y_H_A 0x2B  // high byte y axis register address
#define LSM303_ACC_REG_OUT_Z_L_A 0x2C  // low byte z axis register address
#define LSM303_ACC_REG_OUT_Z_H_A 0x2D  // high byte z axis register address

// multiple bytes read
#define LSM303_ACC_REG_MLTI_OUT_X_A (LSM303_ACC_REG_OUT_X_L_A | 0x80)  // set MSBit to 1 to possible read multiple bytes
#define LSM303_ACC_REG_MLTI_OUT_Y_A (LSM303_ACC_REG_OUT_Y_L_A | 0x80)  // set MSBit to 1 to possible read multiple bytes
#define LSM303_ACC_REG_MLTI_OUT_Z_A (LSM303_ACC_REG_OUT_Z_L_A | 0x80)  // set MSBit to 1 to possible read multiple bytes

#define LSM303_ACC_RESOLUTION 2.0  // maximal acc value [g]

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */

uint8_t LSM303_settings = LSM303_ACC_XYZ_ENABLE | LSM303_ACC_100HZ;
uint32_t I2C_timeout = 100;

// read acc values
uint8_t accReadData[6];

int16_t X_axisAcc = 0;  // acc X raw value
int16_t Y_axisAcc = 0;  // acc Y raw value
int16_t Z_axisAcc = 0;  // acc Z raw value

float X_axisAcc_g = 0;  // acc X value in [g]
float Y_axisAcc_g = 0;  // acc Y value in [g]
float Z_axisAcc_g = 0;  // acc Z value in [g]

// threshold for light LED
const float acc_LED_threshold = 0.5;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  HAL_I2C_Mem_Write(&hi2c1, LSM303_ACC_ADDRESS, LSM303_ACC_CTRL_REG1_A, 1, &LSM303_settings, 1, I2C_timeout);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  // read registers X_L, X_H, Y_L, Y_H, Z_L, Z_H
	  HAL_I2C_Mem_Read(&hi2c1, LSM303_ACC_ADDRESS, LSM303_ACC_REG_MLTI_OUT_X_A, 1, accReadData, 6, I2C_timeout);

	  X_axisAcc = ((accReadData[1] << 8) | accReadData[0]);
	  Y_axisAcc = ((accReadData[3] << 8) | accReadData[2]);
	  Z_axisAcc = ((accReadData[5] << 8) | accReadData[4]);

	  X_axisAcc_g = ((float)X_axisAcc * LSM303_ACC_RESOLUTION) / ((float)INT16_MAX);
	  Y_axisAcc_g = ((float)Y_axisAcc * LSM303_ACC_RESOLUTION) / ((float)INT16_MAX);
	  Z_axisAcc_g = ((float)Z_axisAcc * LSM303_ACC_RESOLUTION) / ((float)INT16_MAX);

	  // light LED on X axis
	  if (X_axisAcc_g > acc_LED_threshold)
	  {
		  HAL_GPIO_WritePin(LED_X_POSITIVE_GPIO_Port, LED_X_POSITIVE_Pin, GPIO_PIN_SET);
	  }
	  else if (X_axisAcc_g < -acc_LED_threshold)
	  {
		  HAL_GPIO_WritePin(LED_X_NEGATIVE_GPIO_Port, LED_X_NEGATIVE_Pin, GPIO_PIN_SET);
	  }
	  else
	  {
		  HAL_GPIO_WritePin(LED_X_POSITIVE_GPIO_Port, LED_X_POSITIVE_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_X_NEGATIVE_GPIO_Port, LED_X_NEGATIVE_Pin, GPIO_PIN_RESET);
	  }


	  // light LED on Y axis
	  if (Y_axisAcc_g > acc_LED_threshold)
	  {
		  HAL_GPIO_WritePin(LED_Y_POSITIVE_GPIO_Port, LED_Y_POSITIVE_Pin, GPIO_PIN_SET);
	  }
	  else if (Y_axisAcc_g < -acc_LED_threshold)
	  {
		  HAL_GPIO_WritePin(LED_Y_NEGATIVE_GPIO_Port, LED_Y_NEGATIVE_Pin, GPIO_PIN_SET);
	  }
	  else
	  {
		  HAL_GPIO_WritePin(LED_Y_POSITIVE_GPIO_Port, LED_Y_POSITIVE_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_Y_NEGATIVE_GPIO_Port, LED_Y_NEGATIVE_Pin, GPIO_PIN_RESET);
	  }


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LED_Y_POSITIVE_Pin|LED_X_POSITIVE_Pin|LED_Y_NEGATIVE_Pin|LED_X_NEGATIVE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LED_Y_POSITIVE_Pin LED_X_POSITIVE_Pin LED_Y_NEGATIVE_Pin LED_X_NEGATIVE_Pin */
  GPIO_InitStruct.Pin = LED_Y_POSITIVE_Pin|LED_X_POSITIVE_Pin|LED_Y_NEGATIVE_Pin|LED_X_NEGATIVE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
